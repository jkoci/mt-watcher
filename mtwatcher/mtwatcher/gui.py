#!/usr/bin/env python3
# coding: utf-8
"""
Tk GUI for Minetest Watcher
"""
import time
import threading


import tkinter as tk
from tkinter.scrolledtext import ScrolledText
from tkinter.ttk import Progressbar, Combobox

from types import SimpleNamespace

INPUTWIDTH = 20
STEP = 0.5


class AppGui(tk.Frame):  # pylint: disable=too-many-ancestors
    """
    The viewer frame. Contains tk variables (self.vars), layout (self.elements) and
    some helper functions (callback generators)
    """

    def __init__(self, app_core, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.core = app_core

        self.vars = SimpleNamespace()
        self.elements = SimpleNamespace()

        # Server address + label
        self.vars.server = tk.StringVar()
        self.vars.server.set(self.core.args.server)
        self.vars.server.trace_add("write", self.push("server"))

        self.elements.serverlbl = tk.Label(self, text="server:port")
        self.elements.serverlbl.grid(sticky="W")

        self.elements.server = Combobox(
            self, width=INPUTWIDTH, textvariable=self.vars.server
        )

        self.elements.server["values"] = [
            self.core.args.config.get(e, "server")
            for e in self.core.args.config.sections()
        ]
        self.elements.server.grid(row=0, column=1, sticky="WE")

        # Poll interval + label
        self.vars.interval = tk.DoubleVar()
        self.vars.interval.set(self.core.args.interval)
        self.vars.interval.trace_add("write", self.push("interval"))

        self.elements.intervallbl = tk.Label(self, text="Interval (secs)")
        self.elements.intervallbl.grid(sticky="W")

        self.elements.interval = tk.Spinbox(
            self, from_=150, to=1200, textvariable=self.vars.interval, width=INPUTWIDTH
        )
        self.elements.interval.grid(row=1, column=1, sticky="WE")

        # Running toggle + label
        self.elements.runbtn = tk.Button(self, text="Run!", command=self.run)
        self.elements.runbtn.grid(row=2, column=0, sticky="W")

        self.vars.run = tk.BooleanVar()
        self.vars.run.trace_add("write", self.push("run"))
        self.elements.run = tk.Checkbutton(self, text="Running?", var=self.vars.run)
        self.elements.run.grid(row=2, column=1, sticky="W")

        # Users list
        self.vars.users = tk.StringVar()
        self.vars.users.trace_add("write", self.settext)

        self.elements.users = ScrolledText(self, width=40, height=10)
        self.elements.users.grid(columnspan=2, sticky="WENS")

        self.vars.progress = tk.IntVar()
        self.vars.progress.trace_add("write", self.progress)

        # Progressbar (time to next poll)
        self.elements.progress = Progressbar(self)
        self.elements.progress.grid(columnspan=2, sticky="WENS")

        self.vars.progress_cur = tk.DoubleVar()

        # weight makes the columns/rows growing when resizing the window
        # R3 contains the scrolledtext view
        # C1 contains the data elements (i.e. not labels)
        tk.Grid.rowconfigure(self, 3, weight=1)
        tk.Grid.columnconfigure(self, 1, weight=1)

    def push(self, key):
        """
        Create the tracing callback for pushing values from Tk world to watcher's args
        """

        def returned(varname, index, mode):  # pylint: disable=unused-argument
            try:
                value = getattr(self.vars, key).get()
                setattr(self.core.args, key, value)
            except tk.TclError:  # When edited to '', we do no action
                pass

        return returned

    def settext(self, varname, index, mode):  # pylint: disable=unused-argument
        """
        Tracing callback for updating the scrolledtext view
        """
        self.elements.users.delete(1.0, tk.END)
        return self.elements.users.insert(tk.INSERT, self.vars.users.get())

    def run(self):
        """
        Run! button callback. When button is pressed, updating of scrolledtext is permitted.
        Update is triggered in next tick by setting the counter over max.
        """
        self.vars.run.set(1)
        self.vars.progress_cur.set(1 + self.core.args.interval)

    def progress(self, varname, index, mode):  # pylint: disable=unused-argument
        """
        Update progressbar when it's value is changed from data watcher
        """
        self.elements.progress["value"] = self.vars.progress.get()


class PollThread(threading.Thread):
    """
    Periodically poll master server, return wanted information.
    """

    def __init__(self, app_core, app_gui, *args, **kwargs):
        self.core = app_core
        self.args = app_core.args

        self.gui = app_gui
        self.tkvars = app_gui.vars
        super().__init__(*args, **kwargs)

    def run(self):
        curr = self.tkvars.progress_cur
        progressbar = self.tkvars.progress
        run = self.tkvars.run

        while True:
            curr.set(0)
            while curr.get() < self.args.interval:
                time.sleep(STEP)
                curr.set(curr.get() + STEP)
                if run.get():
                    progressbar.set(100 * curr.get() / self.args.interval)
                else:
                    progressbar.set(0)

            while not run.get():
                time.sleep(STEP)

            ret = self.core.get_users("\n")
            self.tkvars.users.set(ret)


def run_gui(app_core):
    """
    Build the GUI, launch polling thread, run the GUI
    """
    app = tk.Tk()
    app.title("Minetest Watcher")
    tk.Grid.rowconfigure(app, 0, weight=1)
    tk.Grid.columnconfigure(app, 0, weight=1)

    app_gui = AppGui(app_core, parent=app)
    app_gui.grid(sticky="NSEW")

    app.update()
    app.minsize(app.winfo_width(), app.winfo_height())

    thread = PollThread(app_core, app_gui, daemon=True)
    thread.start()
    app.mainloop()
