#!/usr/bin/env python3
# coding: utf-8
"""
The core of Minetest Watcher. Contains polling logic and json parsing.
"""
import json
import sys

from urllib.request import Request, urlopen
from urllib.parse import urlparse

# pylint: disable=bad-continuation # blacken does this and I'm happy with itl


class AppCore:
    """
    Parses needed information from master server. Holds arguments
    """

    def __init__(self, args):
        self.args = args
        self.last_record = None

    def get_users(self, delimeter=" "):
        """
        Take users list from server record, sort it and return as string
        """
        try:
            self.get_server_info()
            clients = self.last_record["clients_list"]  # APIFLAG
            clients.sort(key=lambda nick: nick.lower())
            return delimeter.join(clients) or "[Server is empty]"
        except ServerOffline as e:
            return "Server {} is offline.".format(str(e))

    def get_server_info(self):
        """
        Find the server record - by testing address/hostname and port
        """
        try:
            address = "minetest://" + self.args.server
        except TypeError:
            print("No server address given, exiting")
            sys.exit(1)
        parsed_host = urlparse(address)

        for record in self.query_master():
            if (
                record["address"] == parsed_host.hostname
                and record["port"] == parsed_host.port
            ):
                self.last_record = record
                return
        raise ServerOffline(self.args.server)

    def query_master(self):
        """
        Request json from master server, convert to dict and pull out only the list of servers
        """
        request = Request(self.args.master)
        with urlopen(request) as fr:
            ret = json.loads(fr.read())

        return ret["list"]


class ServerOffline(Exception):
    "When server record is not found, it is probably offline."
