#!/usr/bin/env python3
# coding: utf-8
"""
Command line interface for Minetest Watcher. Parses arguments, prints to console
"""
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from configparser import ConfigParser
from os import get_terminal_size as term_size
import sys
from textwrap import fill
from pkgutil import get_data

from mtwatcher.gui import run_gui
from mtwatcher.core import AppCore

DEFAULT_CONFIGS = "config.ini"


def main(args=None):
    """
    Parse arugments, configure core, launch
    """
    if args is None:
        args = sys.argv

    parsed = parse_args(args)

    core = AppCore(parsed)
    if parsed.oneshot:
        users = core.get_users(", ")
        cols, _ = term_size()
        print(fill(users, cols))
    else:
        run_gui(core)


def parse_args(argstring):
    """
    Find out defaults, parse arguments from command line
    """
    c = ConfigParser(default_section="common")
    defaults = {}

    cp = ArgumentParser(  # pylint: disable=C0103 # cp=configuration parser
        add_help=False
    )
    cp.add_argument(
        "--config", "-c", help="Specify config file", metavar="FILE", default=None
    )
    cp.add_argument(
        "--section", "-g", help="Specify section of the file", metavar="SECTION"
    )
    known, rest = cp.parse_known_args(argstring)

    if known.config is None:
        data = get_data(__name__, DEFAULT_CONFIGS).decode("utf-8")
        c.read_string(data)
    elif known.config:
        c.read([known.config])

    # update from default section first
    # so the configuration can be grouped or overriden only where needed
    defaults.update(dict(c.items(c.default_section)))
    if known.section:
        defaults.update(dict(c.items(known.section)))

    p = ArgumentParser(  # pylint: disable=C0103 # p=parser
        parents=[cp], formatter_class=ArgumentDefaultsHelpFormatter
    )
    p.add_argument(
        "--interval", "-i", help="Update interval in seconds", type=int, default=600
    )  # It seems the record is updated every 5 minutes
    p.add_argument(
        "--master",
        "-m",
        help="Master server address",
        default="http://servers.minetest.net/list.json",
    )
    p.add_argument("--server", "-s", help="Server address (incl. port)")
    p.add_argument(
        "--oneshot",
        "-1",
        help="Print the list once and exit",
        default=False,
        action="store_true",
    )

    p.set_defaults(**defaults)
    parsed, rest = p.parse_known_args(rest)
    parsed.config = c

    return parsed
