MODULE:=mtwatcher
SOURCE:=$(wildcard *.py)
PROG:=$(MODULE).pyz
CONF:=sample.$(MODULE).ini
ARCHIVE:=$(MODULE).zip

PYTHON?=python3# What python to use for generating .pyz #%
PYTHONSHEBANG?=/usr/bin/env python3# Shebang for generated .pyz #%
PROGARGS?=# These args will be passed to .pyz when calling 'make run' #%

PREFIX?=$$HOME/bin# Where the .pyz will be installed#%
DESTDIR?=# For packaging purposes #%


all: $(ARCHIVE)

archive: $(ARCHIVE) ## build the .pyz archive
$(ARCHIVE): $(PROG) $(CONF)
	zip $@ $?

prog: $(PROG)
$(PROG): $(SOURCE)
	$(PYTHON) -m zipapp --output="$@" --python="$(PYTHONSHEBANG)" \
		--main="$(MODULE).cli:main" "$(MODULE)"

.PHONY: clean clean-bak clean-bin clean-build
clean: clean-bak clean-bin clean-build ## clean intermediate files

clean-bin: ## clean binary files
	-find . -type f  -name '*.pyc' -delete
	-find . -type f -name '*.pyo' -delete
	-find . -type d -name '__pycache__' -delete

clean-bak: ## clean various backup files
	-find . -type f  -name '*~' -delete
	-find . -type f  -name '#*#' -delete

clean-build: ## clean builded files
	-find . -name '$(ARCHIVE)' -delete
	-find . -name '$(PROG)' -delete

.PHONY: run
run: $(PROG) ## runs the program with $(PROGARGS) as arguments
	./"$^" $(PROGARGS)

.PHONY: install
install: $(PROG) ## Install the program to $(DESTDIR)$(PREFIX)
	install "$(PROG)" "$(DESTDIR)$(PREFIX)/$(MODULE)"

.PHONY: help help-vars
help: ## Prints the help
	@grep -E '^[a-zA-Z-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | \
	awk -F':|## ' '{printf "%-15s%s\n", $$1, $$3}'

help-vars: ## Lists "user-altereable" variables
	@(echo "Name=Value=Description";\
	sed -n '/#%$$/ s/#%$$// p' $(MAKEFILE_LIST) | sort) | \
	awk -F'[:?]?=|# ' '{printf "%-15s %-25s%s\n", $$1, $$2, $$3}'

.PHONY: ls
ls: ## List the conents of working tree (exclude .git)
	find . -not -path "./.git/*" -not -path "./.git"
